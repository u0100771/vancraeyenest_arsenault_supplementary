% t_full is the raw data
% t_RGB_rescale,t_Colormap is caculated based on t_full, only for display.
addpath('circularGraph')
close all; clear all;
load('Figure2B.mat');
circularGraph_XL(t_RGB_rescale,t_Colormap,t_full,'Label',ROIname,'Colormap',overlay_cmap);
set(gcf,'Position',[10    10   1200   1200]);
zoom(1.2);
set(gcf,'paperpositionmode','auto');
print(['./circle_syncconn.eps'],'-depsc','-tiff');
