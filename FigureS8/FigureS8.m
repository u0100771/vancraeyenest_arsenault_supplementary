clear all; close all;
load('FigureS8.mat');
figure;
for i = 1:60 
    subplot(6,10,i); hold on;
    plot(NAcVTA_doxoff,ROIs_doxoff(i,:),'b.');lsline;
    plot(NAcVTA_doxon,ROIs_doxon(i,:),'r.');lsline;
    if  tscore(i)> 0
        title([ROIname{1,i},'-',ROIname{2,i}]);
    else 
        title([ROIname{1,i},'-',ROIname{2,i}],'Color','b');
    end
end 
set(gcf,'Position',[10    10   1200   800]);
set(gcf,'paperpositionmode','auto');
print(['./FigS8.eps'],'-depsc','-tiff');

